# SASI Project

Script-ul e dezvoltat cu `Python 3.7`

Pachetele utilizate de acest script pot fi instalate cu urmatoarea comanda:
```
pip install -- user matplotlib, numpy
```

## Procesarea textului de intrare ##

Cu ajutorul argumentului **`processing`** la script, textele de intrare sunt procesate.

Procesarea consta din urmatorii pasi:
- Literele sunt transformate in caracteze mici
- Spatiile sunt eliminate
- Se contorizeaza aparitia literelor si grupurilor carteziene sub forma de procente
- Se creeaza urmatoarele grafice:
    - aparitia literelor ordonate alfabetic 
    - ordonate descrescator in functie de aparitie
    - aparitia grupurilor carteziene ordonate descrescator
- Se creeaza un fisier `.csv` cu procentul de aparitie per litera ordonate alfabetic

Aceasta procesare este individuala pentru fiecare text de intrare dat la rularea script-ului. Practic se poate da unul sau mai multe texte in acelasi timp.

Exemplu de rulare:
```shell
python .\script.py processing .\romanian_text.txt .\english_text.txt .\spanish_text.txt
```

**!Nota** In acest exemplu de rulare, textele de intrare se afla in acelasi director cu script-ul.

Exemplu de output-ul executiei de mai sus:
```
Script-ul va procesa fisierele de intrare.
Dimensiunea fisierului ".\romanian_text.txt" este de "15536" caractere
Dimensiunea fisierului ".\english_text.txt" este de "75158" caractere
Dimensiunea fisierului ".\spanish_text.txt" este de "5395" caractere
Numarul de litere pentru textul ".\romanian_text.txt": 10498".
Numarul de litere pentru textul ".\english_text.txt": 60373".
Numarul de litere pentru textul ".\spanish_text.txt": 4014".
S-a determinat procentul de aparitie per litera
Grupurile de litere din textele de intrare s-au obtinut cu succes.
Script-ul s-a terminat cu succes
```

#### Rezultatele #### 

Graficele sunt salvate in fisiere `.png` in directorul *`results_graphics`* sub numele fisierului de intrare plus un cuvant cheie pentru a simoliza continutul graficului.
Fisierele `.csv` sunt sub numele fisierului de intrare in directorul *`results_csv`*.

**!Nota** Ambele directoare sunt create in directorul din care se ruleaza script-ul (*cwd*).

Exemplu de ierarhie dupa rularea script-ului:
```shell
python .\script.py processing .\romanian_text.txt
```
```
.
├── script.py
├── results_csv
|   └── romanian_text.csv
├── results_graphics
|   ├── romanian_text_cartesian.png
|   ├── romanian_text_raw.png
|   └── romanian_text_ordered.png
└── romanian_text.txt
```

## Compararea textului de intrare ##

Pe parcursul procesarii se ofera in terminal indormatii cu procentajul de similaritate a textului procesat fata de textele cu care a fost antrenat algoritmul.
Antrenarea se face o singura data, moment in care informatiile procesate sunt salvate pe disc in fisiere in folderul **results_csv**.

Acest proces ofera un grafic de asemanare exprimat in procente a textului de intrare cu textele deja procesate.

Exemplu de rulare:

```bash
$ python3 script.py comparing franceza_text.txt
```

Se pot da oricate fisiere de intrare pentru comparare, le vor compara pe rand.

La comparare output-ul este urmatorul:

```txt
Script-ul va compara fisierele de intrare cu datele existente.
Dimensiunea fisierului "franceza_text.txt" este de "10960" caractere
Numarul de litere pentru textul "franceza_text.txt": 8469".
S-a determinat procentul de aparitie per litera
Verify with english_text.csv
Verify with italian_text.csv
Verify with romanian_text.csv
Verify with spanish_text.csv
// ---------------------------------------franceza_text.txt----------------------------------------------- //
Se aseamana cu urmatoarele limbi in proportiile urmatoare:
italian - 130.0%
romanian - 120.0%
spanish - 80.0%
english - 80.0%
Script-ul s-a terminat cu succes
```