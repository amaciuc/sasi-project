import matplotlib.pyplot as plt
import operator
import os
import sys
import string
import numpy as np
import csv

#
known_behav = ["processing", "comparing"]
results_dir = "results_csv"
graphics_dir = "results_graphics"

## -------------------------------------------------------------------------------------------------- ##
def main():
  # Primul argument este reprezinta comportamentul scriptului:
  # -> processing - proceseaza textele de intrare (pot fi mai multe in acelasi timp)
  # -> compare - textul de intrare (doar primul) va fi comparat cu textele procesate
  if len(sys.argv) < 2:
    print("Va rugam sa dati n argument dintre cele doua (processing, comparing)")
    return
  behav = sys.argv[1].strip()
  if behav not in known_behav:
    print(f"Functionalitatea \"{behav}\" nu este cunoscuta. Cele cunoscute sunt: ")
    print(f"\"{known_behav}\". Script-ul se va opri.")
    sys.exit(1)
  if behav == "processing":
    if len(sys.argv) < 3:
      print("Va rugam sa dati si cel putin numele unui fisier")
      return
    print("Script-ul va procesa fisierele de intrare.")
    processing(sys.argv[2:])
  elif behav == "comparing":
    if len(sys.argv) < 3:
      print("Va rugam sa dati si cel putin numele unui fisier")
      return
    print("Script-ul va compara fisierele de intrare cu datele existente.")
    comparing(sys.argv[2:])
  print("Script-ul s-a terminat cu succes")
## -------------------------------------------------------------------------------------------------- ##
def get_contents_of_files(file_names_list):
  content_list = []
  for arg in file_names_list:
    if os.path.isfile(arg):
      input_file = open(arg, encoding="utf-8")
      content = input_file.read().lower().strip()
      content_list.append(content)
      print(f"Dimensiunea fisierului \"{arg}\" este de \"{len(content)}\" "
            "caractere")
    else:
      print(f"Fisierul \"{arg}\" nu exista pe disc. Script-ul se va opri.")
      sys.exit(1)
  return content_list
## -------------------------------------------------------------------------------------------------- ##
def calculate_percentage_for_letters(content_list, file_names_list):
  # Cream dictionarul alfabetului, care va contoriza aparitia fiecarei litere
  alphabet_list = []
  for i in range(0, len(file_names_list)):
    alphabet_list.append(dict.fromkeys(string.ascii_lowercase, 0))
  iter = 0
  count_letter_list = []
  for content in content_list:
    count_letter_list.append(0)
    for item in content:
      if item in alphabet_list[iter]:
        alphabet_list[iter][item] += 1
        count_letter_list[iter] += 1
    print(f"Numarul de litere pentru textul \"{file_names_list[iter]}\": "
          f"{count_letter_list[iter]}\".")
    iter += 1
  iter = 0
  for alphabet in alphabet_list:
    for item in alphabet.keys():
      alphabet_list[iter][item] = float("{0:.2f}".format(
          alphabet[item] / count_letter_list[iter] * 100))
    iter += 1
  print("S-a determinat procentul de aparitie per litera")
  return alphabet_list
## -------------------------------------------------------------------------------------------------- ##
def create_graphics_on_letters(alphabet_list, file_names_list):
  # Grafic cu aparitia literelor
  iter = 0
  for alphabet in alphabet_list:
    file_name = os.path.basename(file_names_list[iter]).split(".")[0]
    csv_file = os.path.join(results_dir, file_name + ".csv")
    try:
      with open(csv_file, 'w') as csv_obj:
        writer = csv.DictWriter(csv_obj, alphabet.keys())
        writer.writeheader()
        writer.writerow(alphabet)
    except IOError:
      print(f"Fisierul \"{csv_file}\" nu poate fi scris. Script-ul se va opri")
      sys.exit(1)
    plt.figure(figsize=(12, 8))
    plt.bar(alphabet.keys(), alphabet.values())
    plt.xlabel("Literele alfabetului")
    plt.ylabel("Procentul de aparitie")
    plt.title('Graficul aparitiei literelor pentru textul de intrare')
    plt.savefig(os.path.join(graphics_dir, file_name + "_raw"))
    # Grafic cu aparitia literelor ordonate
    plt.figure(figsize=(12, 8))
    alphabet_sorted = dict(
        sorted(alphabet.items(), key=lambda x: x[1], reverse=True))
    plt.bar(alphabet_sorted.keys(), alphabet_sorted.values())
    plt.xlabel("Literele alfabetului")
    plt.ylabel("Procentul de aparitie")
    plt.title('Graficul literelor ordonate descrescator')
    plt.savefig(os.path.join(graphics_dir, file_name + "_ordered"))
    iter += 1
## -------------------------------------------------------------------------------------------------- ##
def create_cartesian_list(alphabet_list, content_list):
  cartesian_list = []
  for alphabet in alphabet_list:
    cartesian = {}
    for i in alphabet.keys():
      for j in alphabet.keys():
        cartesian[i + j] = 0
    cartesian_list.append(cartesian)
  # Contorizam perechile din grupul cartezian
  iter = 0
  for content in content_list:
    for i in range(0, len(content) - 1):
      var = content[i] + content[i + 1]
      if var in cartesian_list[iter]:
        cartesian_list[iter][var] += 1
      cartesian_list[iter] = dict(
          sorted(cartesian_list[iter].items(), key=operator.itemgetter(1), reverse=True))
    cartesian_aux = {k: v for k, v in cartesian_list[iter].items() if v is not 0}
    cartesian_list[iter].clear()
    cartesian_list[iter].update(cartesian_aux)
    iter += 1
  print("Grupurile de litere din textele de intrare s-au obtinut cu succes.")
  return cartesian_list
## -------------------------------------------------------------------------------------------------- ##
def plot_cartesian_groups(cartesian_list, file_names_list):
  iter = 0
  for cartesian in cartesian_list:
    plt.figure(figsize=(100, 30))
    plt.bar(cartesian.keys(), cartesian.values())
    plt.xlabel("Literele alfabetului")
    plt.ylabel("Procentul de aparitie")
    plt.title('Graficul aparitiei literelor pentru textul de intrare')
    fig_name = os.path.basename(file_names_list[iter]).split(".")[0]
    plt.savefig(os.path.join(graphics_dir, fig_name + "_cartesian"))
    iter += 1
## -------------------------------------------------------------------------------------------------- ##
def processing(file_names_list):
  if not os.path.exists(results_dir):
    os.mkdir(results_dir)
  if not os.path.exists(graphics_dir):
    os.mkdir(graphics_dir)
  # Preluarea informatiilor din fisiere
  content_list = get_contents_of_files(file_names_list)
  # Parcurgem continutul fisierului si contorizam
  # aparitia fiecarei litere in dictionar
  alphabet_list = calculate_percentage_for_letters(content_list, file_names_list)
  create_graphics_on_letters(alphabet_list, file_names_list)
  # Crearea produsului cartezian
  cartesian_list = create_cartesian_list(alphabet_list, content_list)
  plot_cartesian_groups(cartesian_list, file_names_list)
## -------------------------------------------------------------------------------------------------- ##
def comparing(file_names_list):
  # Pentru fiecare fisier de intrare calculam aceleasi date ca pentru antrenare.
  for new_file in file_names_list:
    language_percentage = dict()
    new_file_text = list()
    new_file_text.append(new_file)
    content_list = get_contents_of_files(list(new_file_text))
    alphabet_list = calculate_percentage_for_letters(content_list, list(new_file_text))
    # Datele obtinute din fiecare fisier de intrare vor fi comparate cu datele cunoscute din antrenare.
    for language_file in os.listdir(results_dir):
      print('Verify with ' + language_file)
      total_percentage = 0
      header = list()
      percentage = list()
      with open(results_dir + '/' + language_file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter = ',')
        counter = 0
        for line in csv_reader:
          if counter == 0:
            header = line
          elif counter == 2:
            percentage = line
          counter += 1
      # Creeaza un dictionar cu litere si procentul de aparitie.
      language = dict()
      for i in range(0, len(header)):
        language[header[i]] = float(percentage[i])
      # alphabet_list - is a dictionar for current file processing.
      # language - is a dictionar for current language data read.
      for key, value in alphabet_list[0].items():
        total_percentage += value - language[key]
      language_percentage[language_file.split('_')[0]] = total_percentage
    # Sortarea dictionarului dupa procente.
    language_percentage = dict(sorted(language_percentage.items(), key=operator.itemgetter(1)))
    print('// ---------------------------------------' + new_file + '----------------------------------------------- //')
    print('Se aseamana cu urmatoarele limbi in proportiile urmatoare:')
    for language, value in language_percentage.items():
      percentage = round((100 - (value * 1000)), 2)
      language_percentage[language] = percentage
      if percentage < 0:
        percentage = 0
      print(language + ' - ' + str(percentage) + '%')
    plt.figure(figsize=(10, 10))
    plt.bar(language_percentage.keys(), language_percentage.values())
    plt.xlabel("Limbile cu care s-a comparat")
    plt.ylabel("Procentul de similaritate")
    plt.title('Graficul similaritatii intre limbi')
    fig_name = new_file + '_out.png'
    plt.savefig(fig_name)
main()